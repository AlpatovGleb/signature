#include <iostream>

using namespace std;

void g()
{
    cout << "   _____   " << endl;
    cout << "  |        " << endl;
    cout << "  |        " << endl;
    cout << "  |    _   " << endl;
    cout << "  |     |  " << endl;
    cout << "  |     |  " << endl;
    cout << "   -----   " << endl;
}

void l()
{
    cout << "  |        " << endl;
    cout << "  |        " << endl;
    cout << "  |        " << endl;
    cout << "  |        " << endl;
    cout << "  |        " << endl;
    cout << "  |        " << endl;
    cout << "   -----   " << endl;
}

void e()
{
    cout << "   _____   " << endl;
    cout << "  |        " << endl;
    cout << "  |        " << endl;
    cout << "  |-----   " << endl;
    cout << "  |        " << endl;
    cout << "  |        " << endl;
    cout << "   -----   " << endl;}

void b()
{
    cout << "    _____   " << endl;
    cout << "   |     |  " << endl;
    cout << "   |_____|  " << endl;
    cout << "   |      | " << endl;
    cout << "   |      | " << endl;
    cout << "   |______| " << endl;
}

int main()
{
    

    g();

    l();
    e();
    b();

    return 0;
}
